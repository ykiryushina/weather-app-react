export const getForecastList = (arr) => {
    let forecastArr = [];
    for (let i = 0; i < arr.length; i++) {
        let days = new Date(arr[i].dt * 1000);
        if (days.getHours() == 15) {
            arr[i].day = days.getDate();
            arr[i].month = days.getMonth() + 1;
            forecastArr.push(arr[i]);
        }
    }
    forecastArr.shift(arr);
    return forecastArr;
}